package com.falabella.hackathon2021.fcsr.model;

import lombok.Data;

@Data
public class RefundRequest {
    String orderId;
    String bankTransactionId;
}
