package com.falabella.hackathon2021.fcsr.controller;

import com.falabella.hackathon2021.fcsr.model.RefundRequest;
import com.falabella.hackathon2021.fcsr.service.FCSRService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/fcsr")
public class FCSRController {

    @Autowired
    FCSRService fcsrService;

    @PostMapping("/initiateRefund")
    public ResponseEntity<?> initiateRefund(@RequestBody RefundRequest refundRequest) {
        return ResponseEntity.ok(fcsrService.initiateRefund(refundRequest));
    }
}
