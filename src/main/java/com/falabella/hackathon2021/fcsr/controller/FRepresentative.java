package com.falabella.hackathon2021.fcsr.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/FRepresentative")
public class FRepresentative {
    @GetMapping("/verifyAndInitiateRefund")
    public ResponseEntity<?> verifyAndInitiateRefund(){
        return ResponseEntity.ok("verifyAndInitiateRefund ok");
    }
}
