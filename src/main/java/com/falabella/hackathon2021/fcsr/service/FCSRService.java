package com.falabella.hackathon2021.fcsr.service;

import com.falabella.hackathon2021.fcsr.model.RefundRequest;

import org.springframework.stereotype.Service;

@Service
public class FCSRService {
    public String initiateRefund(RefundRequest refundRequest) {
        return "Refund request initiated";
    }
}
